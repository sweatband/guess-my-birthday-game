from random import randint

responseName = (input("Hi! What is your name?"))

for currentNumber in range(5):
    currentGuess = currentNumber + 1;
    guessMonth = str(randint(1, 12))
    guessYear = str(randint(1980, 2000))

    print("Guess " + str(currentGuess) + " : " + responseName + " were you born in " + guessMonth + " / " + guessYear + " ?")
    responseAnswer = (input("yes or no? "))

    if responseAnswer == "yes":
        print("I knew it!")
    elif responseAnswer == "no" and currentGuess < 5:
        print ("Drat! Lemme try again")
    else:
        print(" I have other things to do. Good bye. ")
